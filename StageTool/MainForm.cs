﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Renci.SshNet;

namespace StageTool
{
    public partial class MainForm : Form
    {
        private readonly string host = "192.168.1.1";
        private readonly string username = "ubnt";
        private readonly string password = "ubnt";
        private readonly string unms_url = "wss://10.10.1.14:443+UlLU56MMD8K7APbY39GV29AcH5Pk3NxYvaBcnXz9D1CgZ3lz+allowSelfSignedCertificate";
        private readonly List<string> commands_list;

        public MainForm()
        {
            commands_list = new List<string>()
            {
                "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper begin && \n",
                "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper set system host-name erx_id && \n",
                $"/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper set service unms connection {unms_url} && \n",
                "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper delete interfaces ethernet eth0 && \n",
                "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper set interfaces ethernet eth0 address dhcp && \n",
                "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper commit && \n",
                "/opt/vyatta/sbin/vyatta-cfg-cmd-wrapper save && \n",
                "reboot now"
            };
            InitializeComponent();
        }

        private async Task<String> RunAsyncCommands(string commands, SshClient client)
        {
            return await Task.Run(() =>
            {
                return client.RunCommand(commands).Result;
            });
        }

        private string BuildCommands(List<string> commands)
        {
            StringBuilder commands_string = new StringBuilder();
            foreach (string command in commands)
            {
                commands_string.Append(command);
            };
            return commands_string.ToString();
        }

        private void PrintTextBox(string t)
        {
            OutputBox.Clear();
            OutputBox.AppendText(t + Environment.NewLine);
        }

        private void PrintResultTextBox(string t)
        {
            OutputBox.Clear();
            OutputBox.AppendText(t.Replace("\n", "\r\n"));
        }

        private async Task<SshClient> SshConnect(SshClient client)
        {
            return await Task.Run(() => {
                client.HostKeyReceived += (sender, e) => {
                    e.CanTrust = true;
                };
                client.Connect();
                return client;
            });
        }

        private async Task<string> GetID(SshClient client)
        {
            string erx_mac = await RunAsyncCommands("cat /sys/class/net/eth0/address", client);
            return erx_mac.Replace(":", "").Trim();
        }

        private async void Start(object sender, EventArgs e)
        {
            try
            {
                await Main();
            }
            catch
            {
                PrintTextBox("There was a problem connecting to the host...");
            }
        }

        private async Task Main()
        {
            PrintTextBox("Attempting connection...");
            SshClient client = await SshConnect(new SshClient(host, username, password));
            string commands = BuildCommands(commands_list).Replace("erx_id", await GetID(client));
            PrintResultTextBox(commands);
            await RunAsyncCommands(commands, client);
        }
    }
}
